# Corindon::Http
This gem offers an _experimental_ microframework to build simple web application outside of the Rails and global Ruby philosophy where the app depends on global state.

The HTTP Message/Response/Request interfaces are heavily inspired from the PHP-FIG PSR-7 specification to provide an immutable representation of requests.

The application offers a express-like API to define routes and middlewares and contains a Dependency injection controller that can be used to defines services that can then be injected into the request handler block.

## Setup
The require version of Ruby is 3 as we make use of APIs introduced with this version.

No recommended application setup has yet been specified.
However, the minimal setup is:
```ruby
# Gemfile

# hanami-router version 2 is used internally as routing engine. 
gem('hanami-router', github: 'hanami/router', branch: :unstable)

gem('corindon-http', gitlab: 'piotaixr/corindon-http')

# Use thin or any rack-compatible handler
gem 'thin'
```

```ruby
# src/application.rb

require('bundler')
Bundler.setup

require('corindon/http')
require('rack/handler/thin') # Or use the handler you want

# This refinment provides the `ResponseFromString` convenience function
using(Corindon::Http::ResponseExt)

app = Corindon::Http::Application.new

# The first positional param given is an instance of Interfaces::ServerRequest and is optional
app.get('/') do |_request|
  ResponseFromString('Hello world!')
end

# Query params are passed by name to the handler block
app.get('/:name') do |name:|
  ResponseFromString("Hello #{name}!")
end

# Start the application
Rack::Server.start(app: app)
```

You can use [rerun](https://github.com/alexch/rerun) to restart your application on file changes.

## Service injection
Services can be added to the application container. See the documentation of the [Corindon](https://gitlab.com/piotaixr/corindon) gem.
Any injection token provided as named parameter when defining a route are ijnected into the handler block as named parameter
```ruby
# Add here the bootstrap code from the paragraph above

# Our injectable service
class Greeter
  def greet(name)
    "Hello #{name}!"
  end
end

app.container.add_definition(Greeter) # The class uses the default initializer, no need for more configuration.

app.get('/:name', greeter: Greeter) do |greeter:, name:|
  # Don't forget to return an object implementing Interfaces::Response or you will get a HTTP 500
  ResponseFromString(greeter.greet(name)) # no content
end

# Don't forget to start the server
```