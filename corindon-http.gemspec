# frozen_string_literal: true

require_relative('lib/corindon/http/version')

Gem::Specification.new do |spec|
  spec.name = 'corindon-http'
  spec.version = Corindon::Http::VERSION
  spec.summary = 'HTTP framework'
  spec.authors = ['Rémi Piotaix']
  spec.email = ['remi@piotaix.fr']

  spec.license = 'LGPL-3.0-only'

  spec.homepage = 'https://gitlab.com/piotaixr/corindon-http'
  spec.required_ruby_version = '>= 3.0.0'

  spec.files = Dir.glob(%w[lib/**/*.rb corindon-http.gemspec])

  spec.add_dependency('corindon', '~> 0.8.0')
  spec.add_dependency('hanami-router', '2.0.0.alpha4')
  spec.add_dependency('rack', '~> 2.2')
  spec.add_dependency('zeitwerk', '~> 2.3')

  spec.add_development_dependency('bundler')
  spec.add_development_dependency('minitest', '~>  5.14')
  spec.add_development_dependency('minitest-reporters', '~> 1.4')
  spec.add_development_dependency('rake', '~> 13.0')
  spec.add_development_dependency('rubocop', '1.11.0')
  spec.add_development_dependency('thin')
end
