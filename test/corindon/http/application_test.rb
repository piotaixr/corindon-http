# frozen_string_literal: true

require('test_helper')

module Corindon
  module Http
    describe(Application) do
      before do
        @app = Application.new
      end

      it('turns a rack env into a rack response') do
        response = @app.handle(Rack::MockRequest.env_for('/'))

        assert(response.is_a?(Array))
        assert_equal(3, response.length)
      end

      it('transforms the response from the block to a Rack response') do
        @app.get('/') do
          Response.new(
            status: 400,
            body: StringIO.new('Hello world'),
            headers: { "X-foo": '42' }
          )
        end

        status, headers, = @app.handle(Rack::MockRequest.env_for('/'))

        assert_equal(400, status)
        assert_equal({ "X-foo": '42' }, headers)
      end

      it('returns a response with 404 code if no block') do
        status, = @app.handle(Rack::MockRequest.env_for('/'))

        assert_equal(404, status)
      end

      it('selects the block to call based on the path of the request') do
        called = false

        @app.get('/hello') do
          called = true

          Response.new
        end

        @app.get('/') do
          raise(StandardError.new('This block should not be called'))
        end

        response = @app.handle(Rack::MockRequest.env_for('/hello'))

        assert(called)
        assert(response)
      end

      it('passes a ServerRequest object as parameter') do
        called = false

        @app.get('/') do |request|
          called = true

          assert(request.is_a?(Interfaces::ServerRequest))

          Response.new
        end

        @app.handle(Rack::MockRequest.env_for('/'))

        assert(called)
      end

      it('returns a 500 error when the block does not return a Response object') do
        called = false

        @app.get('/') do
          called = true
        end

        status, = @app.handle(Rack::MockRequest.env_for('/'))

        assert(called)
        assert_equal(500, status)
      end

      it('returns a 500 error when a StandardError is raised inside the block') do
        called = false

        @app.get('/') do
          called = true
          raise(StandardError.new('RAISE'))
        end

        status, = @app.handle(Rack::MockRequest.env_for('/'))

        assert(called)
        assert_equal(500, status)
      end

      it('has compatibility with Rack by defining the call method') do
        response = Response.new(status: 201)

        @app.get('/') do
          response
        end

        status, = @app.call(Rack::MockRequest.env_for('/'))

        assert_equal(201, status)
      end

      describe('methods') do
        it('allows to handle POST requests') do
          @app.get('/') do
            raise(StandardError.new('Should not be called'))
          end

          called = false

          @app.post('/') do |request|
            called = true

            assert_equal('POST', request.method)
          end

          @app.handle(
            Rack::MockRequest.env_for('/', method: Rack::POST)
          )

          assert(called)
        end
      end

      describe('query params') do
        it('allows to define query params as part of the path') do
          called = false
          @app.get('/hello/:name') do |request|
            called = true

            qp = request.query_params
            assert(qp.key?('name'))
            assert_equal('foo', qp['name'])
          end

          @app.handle(Rack::MockRequest.env_for('/hello/foo'))

          assert(called)
        end

        it('allow to provide URI query params') do
          called = false
          @app.get('/') do |request|
            called = true

            qp = request.query_params
            assert(qp.key?('name'))
            assert_equal('foo', qp['name'])
          end

          @app.handle(Rack::MockRequest.env_for('/?name=foo'))

          assert(called)
        end
      end

      describe(:middlewares) do
        it('allow the definition of middlewares') do
          @app.use(DummyMiddleware.new)
        end

        it('calls the middleware for each route of the application') do
          middleware = DummyMiddleware.new
          @app.use(middleware)

          called = false
          @app.get('/') do
            called = true

            Response.new
          end

          request = Rack::MockRequest.env_for('/')
          @app.handle(request)

          assert(called)
          assert(middleware.called)
        end
      end

      describe('container') do
        it('has a DI container') do
          assert(@app.container.is_a?(DependencyInjection::Container))
        end
      end

      describe(:injection) do
        it('adds its "container" attribute to the request') do
          called = false

          @app.get('/') do |request|
            called = true
            container = request.attribute('container')

            assert(request.attributes.key?('container'))
            assert(container.is_a?(Corindon::DependencyInjection::Container))
            assert_same(@app.container, container)
          end

          @app.handle(Rack::MockRequest.env_for('/'))

          assert(called)
        end

        it('injects query parameters as kwargs if they exist with the same name in the provided block') do
          called = false

          @app.get('/:name') do |name:, q:|
            called = true

            assert_equal('toto', name)
            assert_equal('42', q)

            Response.new
          end

          status, = @app.handle(Rack::MockRequest.env_for('/toto?q=42'))

          assert_equal(200, status)
          assert(called)
        end

        it('uses kwargs given on definition as injection token for services to be provided to the block') do
          @app.container.add_definition(DummyClass) do
            args(value(42))
          end
          called = false

          @app.get('/', dummy: DummyClass) do |dummy:|
            called = true

            assert(dummy.is_a?(DummyClass))
            assert_equal(42, dummy.value)
          end

          @app.handle(Rack::MockRequest.env_for('/'))

          assert(called)
        end
      end
    end

    class DummyClass
      attr_reader(:value)

      def initialize(value)
        @value = value
      end
    end

    class DummyMiddleware
      include(Corindon::Http::Interfaces::Middleware)

      attr_reader(:called)

      def initialize
        @called = false
      end

      def handle(request, &app)
        @called = true

        app.call(request)
      end
    end
  end
end
