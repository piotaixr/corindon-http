# frozen_string_literal: true

require('test_helper')

require_relative('message_test_mixin')

using(Corindon::Guards::Ext)

module Corindon
  module Http
    class RequestTest < Minitest::Test
      include(MessageEqualityMixin)
      include(MessageTestMixin)

      def setup
        self.message = Request.new
      end

      def test_it_is_initialized_with_sensible_defaults
        m = get_message

        assert_equal('1.0', m.protocol_version)
        assert_equal({}, m.headers)
        assert_equal('', m.body.read)
        assert_equal('/', m.request_target)
        assert_equal('GET', m.method)
        assert(m.uri.is_a?(URI))
      end

      def test_request_target_equality
        r1 = get_message.with_request_target('/foo')
        r2 = get_message.with_request_target('/foo')
        r3 = get_message.with_request_target('/bar')

        assert_equal(r1, r2)
        refute_equal(r1, r3)
      end

      def test_method_equality
        r1 = get_message.with_method('POST')
        r2 = get_message.with_method('POST')
        r3 = get_message.with_method('DELETE')

        assert_equal(r1, r2)
        refute_equal(r1, r3)
      end

      def test_uri_equality
        r1 = get_message.with_uri(URI.parse('/foobar'))
        r2 = get_message.with_uri(URI.parse('/foobar'))
        r3 = get_message.with_uri(URI.parse('/foobarbaz'))

        assert_equal(r1, r2)
        refute_equal(r1, r3)
      end

      def test_request_target
        initial = get_message.clone
        request = initial.with_request_target('*')

        refute_same(initial, request)
        assert_equal(initial, get_message, 'Request object MUST not be mutated')

        assert_equal('*', request.request_target)
      end

      def test_method
        initial = get_message.clone
        request = initial.with_method('POST')

        refute_same(initial, request)
        assert_equal(initial, get_message, 'Request object MUST not be mutated')

        assert_equal('POST', request.method)
      end

      def test_method_is_case_sensitive
        request = get_message.with_method('head')
        assert_equal('head', request.method)
      end

      def test_method_is_extendable
        request = get_message.with_method('CUSTOM')
        assert_equal('CUSTOM', request.method)
      end

      def invalid_method_arguments
        [
          nil,
          1,
          1.01,
          false,
          ['foo'],
          Object.new
        ]
      end

      def test_method_with_invalid_arguments
        invalid_method_arguments.each do |method|
          assert_raises(ArgumentError) do
            get_message.with_method(method)
          end
        end
      end

      def test_uri
        initial = get_message.clone
        uri = URI.parse('http://www.foo.com/bar')
        request = initial.with_uri(uri)

        refute_same(initial, request)
        assert_equal(initial, get_message, 'Request object MUST not be mutated')

        assert_equal(uri, request.uri)
        assert_equal('www.foo.com', request.header_line('host'))

        request2 = request.with_uri(URI.parse('/foobar'))
        assert_equal(
          'www.foo.com',
          request2.header_line('host'),
          'If the URI does not contain a host component, any pre-existing Host header MUST be carried over to the returned request.'
        )
        assert_equal('/foobar', request2.uri.to_s)
      end

      def test_uri_preserve_host_nohost_host
        request = get_message.with_uri(URI.parse('http://www.foo.com/bar'), preserve_host: true)
        assert_equal('www.foo.com', request.header_line('Host'))
      end

      def test_uri_preserve_host_nohost_nohost
        host = get_message.header_line('host')

        request = get_message.with_uri(URI.parse('/bar'), preserve_host: true)
        assert_equal(host, request.header_line('Host'))
      end

      def test_uri_preserve_host_host_host
        request = get_message.with_uri(URI.parse('http://www.foo.com/bar'))
        request2 = request.with_uri(URI.parse('http://www.bar.com/bar'), preserve_host: true)

        assert_equal(request.header_line('host'), request2.header_line('host'))
      end
    end
  end
end
