# frozen_string_literal: true

require('test_helper')

require_relative('message_test_mixin')

using(Corindon::Guards::Ext)

module Corindon
  module Http
    class ServerRequestTest < Minitest::Test
      include(MessageEqualityMixin)
      include(MessageTestMixin)

      def setup
        self.message = ServerRequest.new
      end

      def test_it_is_initialized_with_sensible_defaults
        m = get_message

        assert_equal('1.0', m.protocol_version)
        assert_equal({}, m.headers)
        assert_equal('', m.body.read)
        assert_equal('/', m.request_target)
        assert_equal('GET', m.method)
        assert(m.uri.is_a?(URI))
        assert_equal({}, m.attributes)
        assert_equal({}, m.query_params)
      end

      def test_attributes_influences_equality
        m = get_message

        m1 = m.with_attribute('foo', 'bar')
        m2 = m.with_attribute('foo', 'bar')
        m3 = m.with_attribute('foo', 'BAR')

        assert_equal(m1, m2)
        refute_equal(m1, m3)
      end

      def test_query_params_influences_equality
        m = get_message

        m1 = m.with_query_params(foo: 'bar')
        m2 = m.with_query_params(foo: 'bar')
        m3 = m.with_query_params(foo: 'BAR')

        assert_equal(m1, m2)
        refute_equal(m1, m3)
      end

      def test_attributes
        initial = get_message.clone
        request = initial.with_attribute('foo', 'bar')

        refute_same(initial, request)
        assert_equal(initial, get_message, 'Request object MUST not be mutated')

        assert_equal({ 'foo' => 'bar' }, request.attributes)

        request2 = request.with_attribute('baz', 'biz')
        assert_equal({ 'foo' => 'bar', 'baz' => 'biz' }, request2.attributes)
      end

      def test_attribute
        request = get_message.with_attribute('foo', 'bar')

        assert_equal('bar', request.attribute('foo'))

        assert_equal('default', request.attribute('not found', 'default'))
      end

      def test_without_attribute
        source = get_message.with_attribute('foo', 'bar')
        initial = source.clone
        request = initial.without_attribute('foo')

        refute_same(initial, request)
        assert_equal(initial, source, 'Request object MUST not be mutated')

        assert_nil(request.attribute('foo'))
      end

      describe(:from_rack_env) do
        it('returns a ServerRequest') do
          assert(ServerRequest.from_rack_env({}).is_a?(Interfaces::ServerRequest))
        end

        it('takes the method from the env') do
          env = { Rack::REQUEST_METHOD => 'POST' }
          request = ServerRequest.from_rack_env(env)

          assert_equal('POST', request.method)
        end

        it('takes the protocol version from the env') do
          env = { Rack::SERVER_PROTOCOL => 'HTTP/1.1' }
          request = ServerRequest.from_rack_env(env)

          assert_equal('1.1', request.protocol_version)
        end

        it('takes the request target (path_info) from the env') do
          env = { Rack::PATH_INFO => '/hello' }
          request = ServerRequest.from_rack_env(env)

          assert_equal('/hello', request.request_target)
        end

        it('takes the uri from the env') do
          env = { 'REQUEST_URI' => 'http://localhost/hello' }
          request = ServerRequest.from_rack_env(env)

          assert(request.uri.is_a?(URI))
          assert_equal('http://localhost/hello', request.uri.to_s)
        end

        it('takes the headers from the env') do
          env = {
            'HTTP_ACCEPT' => 'accept_header',
            'HTTP_USER_AGENT' => 'ua_string'
          }
          request = ServerRequest.from_rack_env(env)

          assert(request.has_header?('Accept'))
          assert_equal('accept_header', request.header('Accept'))
          assert(request.has_header?('User-Agent'))
          assert_equal('ua_string', request.header('User-Agent'))
        end
      end
    end
  end
end
