# frozen_string_literal: true

require('test_helper')

using(Corindon::Guards::Ext)

module Corindon
  module Http
    module MessageMixinBase
      def get_message
        @message
      end

      def message=(message)
        @message = message
      end
    end

    module MessageInitializationMixin
      include(MessageMixinBase)
    end

    module MessageEqualityMixin
      include(MessageMixinBase)

      def test_duplicates_are_equal
        m = get_message
        assert_equal(m, m.dup)
      end

      def test_protocol_version_influences_equality
        m = get_message
        m1 = m.with_protocol_version('1.0')
        m2 = m.with_protocol_version('1.0')
        m3 = m.with_protocol_version('1.1')

        assert_equal(m1, m2)
        refute_equal(m1, m3)
      end

      def test_body_influences_equality
        m = get_message
        stream = StringIO.new
        stream2 = StringIO.new

        m1 = m.with_body(stream)
        m2 = m.with_body(stream)
        m3 = m.with_body(stream2)

        assert_equal(m1, m2)
        refute_equal(m1, m3)
      end

      def test_headers_influences_equality
        m = get_message

        m1 = m.with_header('foo', 'bar')
        m2 = m.with_header('foo', 'bar')
        m3 = m.with_header('foo', 'baz')
        m4 = m.with_header('Foo', 'bar')

        assert_equal(m1, m2)
        refute_equal(m1, m3)
        refute_equal(m1, m4) # Not sure about this.
      end

      def test_initialize_set_header_names
        m = Message.new(
          headers: { 'ACCEPT' => 'accept_string' }
        )

        assert(m.has_header?('Accept'))
      end
    end

    module MessageTestMixin
      include(MessageMixinBase)

      def test_protocol_version
        initial_message = get_message
        original = initial_message.dup

        new_message = initial_message.with_protocol_version('1.0')

        refute_same(initial_message, new_message)
        assert_equal(initial_message, original, 'Message object MUST not be mutated')

        assert_equal('1.0', new_message.protocol_version)
      end

      def test_headers
        initial_message = get_message
        original = initial_message.dup

        message = initial_message.with_added_header('content-type', 'text/html')
                                 .with_added_header('content-type', 'text/plain')

        refute_same(initial_message, message)
        assert_equal(initial_message, original, 'Message object MUST not be mutated')

        headers = message.headers

        assert(headers.frozen?)
        assert(headers.key?('content-type'))
        assert_equal(2, headers['content-type'].size)
        assert_includes(headers['content-type'], 'text/plain')
        assert_includes(headers['content-type'], 'text/html')
      end

      def test_has_header
        message = get_message.with_added_header('content-type', 'text/html')

        assert(message.has_header?('content-type'))
        assert(message.has_header?('Content-Type'))
        assert(message.has_header?('CONTENT-TYPE'))
      end

      def test_header
        m = get_message.with_header('content-type', 'text/html')

        assert_equal(['text/html'], m.header('content-type'))
        assert_equal(['text/html'], m.header('Content-Type'))
        assert_equal(['text/html'], m.header('CONTENT-TYPE'))
        assert(m.header('content-type').frozen?)

        not_present = m.header('x-not-present')
        assert(not_present.is_a?(Array))
        assert_equal(0, not_present.size)
      end

      def test_header_line
        m = get_message.with_header('content-type', %w[text/html text/plain])

        assert_equal('text/html, text/plain', m.header_line('content-type'))
        assert_equal('text/html, text/plain', m.header_line('Content-Type'))
        assert_equal('text/html, text/plain', m.header_line('CONTENT-TYPE'))

        assert_equal('', m.header_line('x-not-present'))
      end

      def test_with_header
        initial_message = get_message
        original = initial_message.dup

        new_message = initial_message.with_header('content-type', 'text/html')

        refute_same(initial_message, new_message)
        assert_equal(initial_message, original, 'Message object MUST not be mutated')
        assert_equal('text/html', new_message.header_line('content-type'))

        new_message = initial_message.with_header('content-type', 'text/plain')
        assert_equal('text/plain', new_message.header_line('content-type'))

        new_message = initial_message.with_header('Content-TYPE', 'text/script')
        assert_equal('text/script', new_message.header_line('content-type'))

        new_message = initial_message.with_header('x-foo', %w[bar baz])
        assert_equal('bar, baz', new_message.header_line('x-foo'))

        new_message = initial_message.with_header('Bar', '')
        assert(new_message.has_header?('Bar'))
        assert_equal([''], new_message.header('Bar'))
      end

      def invalid_headers
        [
          [[], 'foo'],
          ['foo', []],
          ['', ''],
          ['foo', false],
          [false, 'foo'],
          ['foo', Object.new],
          [Object.new, 'foo']
        ]
      end

      def test_with_header_invalid_argument
        invalid_headers.each do |name, value|
          assert_raises(ArgumentError) do
            get_message.with_header(name, value)
          end
        end
      end

      def test_with_added_header
        m = get_message.with_added_header('content-type', 'text/html')
                       .with_added_header('CONTENT-type', 'text/plain')

        assert_equal('text/html, text/plain', m.header_line('content-type'))
        assert_equal('text/html, text/plain', m.header_line('Content-Type'))
      end

      def test_with_added_header_invalid_argument
        invalid_headers.each do |name, value|
          assert_raises(ArgumentError) do
            get_message.with_added_header(name, value)
          end
        end
      end

      def test_with_added_header_array_value
        m = get_message.with_added_header('content-type', 'text/html')
        message = m.with_added_header('content-type', %w[text/plain application/json])

        line = message.header_line('content-type')
        assert_match(%r{text/html}, line)
        assert_match(%r{text/plain}, line)
        assert_match(%r{application/json}, line)
      end

      def test_without_header
        m = get_message.with_added_header('content-type', 'text/html')
                       .with_added_header('Age', '0')
                       .with_added_header('X-foo', 'bar')

        assert(m.headers.key?('Age'))
        assert_equal(['0'], m.headers.fetch('Age'))

        m2 = m.without_header('age')
        assert_equal(m.headers.size - 1, m2.headers.size)
        refute(m2.headers.key?('Age'))
      end

      def test_body
        initial_message = get_message
        original = initial_message.dup

        stream = StringIO.new('Hello world')
        message = initial_message.with_body(stream)

        refute_same(initial_message, message)
        assert_equal(initial_message, original, 'Message object MUST not be mutated')

        assert_equal(stream, message.body)
      end
    end
  end
end
