# frozen_string_literal: true

require('test_helper')

require_relative('message_test_mixin')

using(Corindon::Guards::Ext)

module Corindon
  module Http
    class ResponseTest < Minitest::Test
      include(MessageEqualityMixin)
      include(MessageTestMixin)

      def setup
        self.message = Response.new
      end

      def test_it_is_initialized_with_sensible_defaults
        m = get_message

        assert_equal('1.0', m.protocol_version)
        assert_equal({}, m.headers)
        assert_equal('', m.body.read)
        assert_equal(200, m.status)
      end

      def test_status
        initial = get_message.clone
        request = initial.with_status(204)

        refute_same(initial, request)
        assert_equal(initial, get_message, 'Request object MUST not be mutated')

        assert_equal(204, request.status)
      end

      def invalid_status_codes
        [
          true,
          'foobar',
          99,
          600,
          200.34,
          Object.new
        ]
      end

      def test_status_invalid_argument
        invalid_status_codes.each do |status|
          assert_raises(ArgumentError) do
            get_message.with_status(status)
          end
        end
      end
    end
  end
end
