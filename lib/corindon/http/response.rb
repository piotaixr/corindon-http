# frozen_string_literal: true

module Corindon
  module Http
    class Response < Message
      include(Interfaces::Response)

      attr_reader(:status)

      def initialize(
        protocol_version: '1.0',
        headers: {}.freeze,
        body: StringIO.new,
        status: 200
      )
        super(
          protocol_version: protocol_version,
          headers: headers,
          body: body,
        )

        self.status = status
      end

      def with_status(status)
        if status.is_a?(Integer) && 100 <= status && status < 600

          message = clone
          message.status = status

          message
        else
          raise(ArgumentError.new("Status codes should be Integer between [100 and 600[, given: '#{status}'"))
        end
      end

      protected

        attr_writer(:status)
    end
  end
end
