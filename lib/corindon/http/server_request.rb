# frozen_string_literal: true

module Corindon
  module Http
    class ServerRequest < Request
      include(Interfaces::ServerRequest)

      class << self
        def from_rack_env(env)
          headers = env.filter { |k, _v| k =~ /\AHTTP_/ }
                       .transform_keys { |k| k.sub(/\AHTTP_/, '').sub('_', '-') }

          ServerRequest.new(
            method: env.fetch(Rack::REQUEST_METHOD, 'GET'),
            protocol_version: env.fetch(Rack::SERVER_PROTOCOL, '1.0').sub(%r{HTTP/}, ''),
            request_target: env.fetch(Rack::PATH_INFO, '/'),
            uri: URI.parse(env.fetch('REQUEST_URI', '/')),
            headers: headers
          )
        end
      end

      attr_reader(:attributes)
      attr_reader(:query_params)

      def initialize(
        protocol_version: '1.0',
        headers: {}.freeze,
        body: StringIO.new,
        request_target: '/',
        method: 'GET',
        uri: URI.parse(''),
        attributes: {},
        query_params: {}
      )
        super(
          protocol_version: protocol_version,
          headers: headers,
          body: body,
          request_target: request_target,
          method: method,
          uri: uri
        )

        self.attributes = attributes
        self.query_params = query_params
      end

      def eql?(other)
        super(other) &&
          attributes == other.attributes &&
          query_params == other.query_params
      end

      def with_attribute(name, value)
        message = clone
        message.attributes = { **attributes, name => value }

        message
      end

      def attribute(name, default = nil)
        attributes.fetch(name, default)
      end

      def without_attribute(name)
        message = clone
        message.attributes = attributes.except(name)

        message
      end

      def with_query_params(**params)
        message = clone
        message.query_params = params.transform_keys(&:to_s)

        message
      end

      protected

        attr_writer(:attributes)
        attr_writer(:query_params)
    end
  end
end
