# frozen_string_literal: true

module Corindon
  module Http
    module ResponseExt
      refine(Object) do
        def ResponseFromString(body, status: 200)
          Response.new(status: status, body: StringIO.new(body))
        end
      end
    end
  end
end
