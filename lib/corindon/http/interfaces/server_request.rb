# frozen_string_literal: true

# rubocop:disable Lint/UnusedMethodArgument

using(Corindon::Guards::Ext)

module Corindon
  module Http
    module Interfaces
      module ServerRequest
        include(Request)

        # @return [Hash{String=>Object}]
        def attributes
          unimplemented!
        end

        # @param [String] name
        # @param [Object, nil] default
        # @return [Object, nil]
        def attribute(name, default = nil)
          unimplemented!
        end

        # @param [String] name
        # @param [Object] value
        # @return [self]
        def with_attribute(name, value)
          unimplemented!
        end

        # @param [String] name
        # @return [name]
        def without_attribute(name)
          unimplemented!
        end

        # @return [Hash{String=>Object}]
        def query_params
          unimplemented!
        end

        # @param [Hash] params
        # @return [self]
        def with_query_params(**params)
          unimplemented!
        end
      end
    end
  end
end
# rubocop:enable Lint/UnusedMethodArgument
