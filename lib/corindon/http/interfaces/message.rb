# frozen_string_literal: true

# rubocop:disable Lint/UnusedMethodArgument

using(Corindon::Guards::Ext)

module Corindon
  module Http
    module Interfaces
      # HTTP messages consist of requests from a client to a server and responses
      # from a server to a client. This interface defines the methods common to
      # each.
      #
      # Messages are considered immutable; all methods that might change state MUST
      # be implemented such that they retain the internal state of the current
      # message and return an instance that contains the changed state.
      #
      # @see http://www.ietf.org/rfc/rfc7230.txt
      # @see http://www.ietf.org/rfc/rfc7231.txt
      module Message
        # Retrieves the HTTP protocol version as a string.
        #
        # The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
        #
        # @return [String] HTTP protocol version.
        def protocol_version
          unimplemented!
        end

        # Return an instance with the specified HTTP protocol version.
        #
        # The version string MUST contain only the HTTP version number (e.g.,
        # "1.1", "1.0").
        #
        # This method MUST be implemented in such a way as to retain the
        # immutability of the message, and MUST return an instance that has the
        # new protocol version.
        #
        # @param [String] version HTTP protocol version
        # @return [self]
        def with_protocol_version(version)
          unimplemented!
        end

        # Retrieves all message header values.
        #
        # The keys represent the header name as it will be sent over the wire, and
        # each value is an array of strings associated with the header.
        #
        #     // Represent the headers as a string
        #     message.headers.each do |name, values|
        #       puts("#{name}: #{values.join(', ')}")
        #     end
        #
        #     // Emit headers iteratively:
        #     message.headers.each do |name, values|
        #       values.each do |value|
        #         puts("#{name}: #{value}")
        #       end
        #     end
        #
        # While header names are not case-sensitive, `headers` will preserve the
        # exact case in which headers were originally specified.
        #
        # @return [Hash{String=>Array<String>}] Returns Hash of the message's headers.
        #     Each key MUST be a header name, and each value MUST be an array of
        #     strings for that header.
        def headers
          unimplemented!
        end

        # Checks if a header exists by the given case-insensitive name.
        #
        # @param [String] name Case-insensitive header field name.
        # @return [Boolean] Returns true if any header names match the given header
        #     name using a case-insensitive string comparison. Returns false if
        #     no matching header name is found in the message.
        def has_header?(name)
          unimplemented!
        end

        # Retrieves a message header value by the given case-insensitive name.
        #
        # This method returns an array of all the header values of the given
        # case-insensitive header name.
        #
        # If the header does not appear in the message, this method MUST return an
        # empty array.
        #
        # @param [String] name Case-insensitive header field name.
        # @return [Array<String>] An array of string values as provided for the given
        #    header. If the header does not appear in the message, this method MUST
        #    return an empty array.
        def header(name)
          unimplemented!
        end

        # Retrieves a comma-separated string of the values for a single header.
        #
        # This method returns all of the header values of the given
        # case-insensitive header name as a string concatenated together using
        # a comma.
        #
        # NOTE: Not all header values may be appropriately represented using
        # comma concatenation. For such headers, use header() instead
        # and supply your own delimiter when concatenating.
        #
        # If the header does not appear in the message, this method MUST return
        # an empty string.
        #
        # @param [String] $name Case-insensitive header field name.
        # @return [String] A string of values as provided for the given header
        #    concatenated together using a comma. If the header does not appear in
        #    the message, this method MUST return an empty string.
        def header_line(name)
          unimplemented!
        end

        # Return an instance with the provided value replacing the specified header.
        #
        # While header names are case-insensitive, the casing of the header will
        # be preserved by this function, and returned from `headers`.
        #
        # This method MUST be implemented in such a way as to retain the
        # immutability of the message, and MUST return an instance that has the
        # new and/or updated header and value.
        #
        # @param [String] name Case-insensitive header field name.
        # @param [String, Array<String>] value Header value(s).
        # @return [self]
        def with_header(name, value)
          unimplemented!
        end

        # Return an instance with the specified header appended with the given value.
        #
        # Existing values for the specified header will be maintained. The new
        # value(s) will be appended to the existing list. If the header did not
        # exist previously, it will be added.
        #
        # This method MUST be implemented in such a way as to retain the
        # immutability of the message, and MUST return an instance that has the
        # new header and/or value.
        #
        # @param [String] name Case-insensitive header field name to add.
        # @param [String, Array<String>] value Header value(s).
        # @return [self]
        def with_added_header(name, value)
          unimplemented!
        end

        # Return an instance without the specified header.
        #
        # Header resolution MUST be done without case-sensitivity.
        #
        # This method MUST be implemented in such a way as to retain the
        # immutability of the message, and MUST return an instance that removes
        # the named header.
        #
        # @param [String] name Case-insensitive header field name to remove.
        # @return [self]
        def without_header(name)
          unimplemented!
        end

        # Gets the body of the message.
        #
        # @return [Interfaces::Stream] Returns the body as a stream.
        def body
          unimplemented!
        end

        # Return an instance with the specified message body.
        #
        # The body MUST be a StreamInterface object.
        #
        # This method MUST be implemented in such a way as to retain the
        # immutability of the message, and MUST return a new instance that has the
        # new body stream.
        #
        # @param [Interfaces::Stream] body Body.
        # @return [self]
        def with_body(body)
          unimplemented!
        end
      end
    end
  end
end
# rubocop:enable Lint/UnusedMethodArgument
