# frozen_string_literal: true

# rubocop:disable Lint/UnusedMethodArgument

using(Corindon::Guards::Ext)

module Corindon
  module Http
    module Interfaces
      module Middleware
        def handle(request, &app)
          unimplemented!
        end
      end
    end
  end
end

# rubocop:enable Lint/UnusedMethodArgument
