# frozen_string_literal: true

# rubocop:disable Lint/UnusedMethodArgument

using(Corindon::Guards::Ext)

module Corindon
  module Http
    module Interfaces
      module Request
        include(Message)

        def uri
          unimplemented!
        end

        def with_uri(uri, preserve_host: false)
          unimplemented!
        end

        def request_target
          unimplemented!
        end

        def with_request_target(target)
          unimplemented!
        end

        def method
          unimplemented!
        end

        def with_method(method)
          unimplemented!
        end
      end
    end
  end
end
# rubocop:enable Lint/UnusedMethodArgument
