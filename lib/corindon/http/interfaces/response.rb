# frozen_string_literal: true

# rubocop:disable Lint/UnusedMethodArgument

module Corindon
  module Http
    module Interfaces
      module Response
        include(Message)

        # @return [Integer]
        def status
          unimplemented!
        end

        # @param [Integer] status
        # @return [self]
        def with_status(status)
          unimplemented!
        end
      end
    end
  end
end
# rubocop:enable Lint/UnusedMethodArgument
