# frozen_string_literal: true

module Corindon
  module Http
    class Message
      include(Interfaces::Message)

      def initialize(protocol_version: '1.0', headers: {}, body: StringIO.new)
        self.protocol_version = protocol_version
        self.headers = { **headers }.freeze
        self.header_names = headers.keys.to_h { |k| [k.downcase, k] }
        self.body = body
      end

      def with_protocol_version(version)
        message = clone
        message.protocol_version = version
        message
      end

      def eql?(other)
        self.class == other.class &&
          protocol_version == other.protocol_version &&
          body == other.body &&
          headers == other.headers
      end

      def ==(other)
        eql?(other)
      end

      def with_header(name, value)
        if !valid_name(name)
          raise(ArgumentError.new("Name #{name} is not valid"))
        elsif !valid_value(value)
          raise(ArgumentError.new("Value #{value} is not valid"))
        else
          message = clone
          message.headers = message.headers.merge(message.header_name_for(name) => [*Array(value)].freeze).freeze

          message
        end
      end

      def with_added_header(name, value)
        if !valid_name(name)
          raise(ArgumentError.new('Name is not valid'))
        elsif !valid_value(value)
          raise(ArgumentError.new('Value is not valid'))
        else
          message = clone
          message.headers = message.headers.merge(
            message.header_name_for(name) => [*message.header(name.downcase), *Array(value)].freeze
          ).freeze

          message
        end
      end

      def has_header?(name)
        headers.key?(header_names.fetch(name.downcase, nil))
      end

      def header_line(name)
        header(name).join(', ')
      end

      def header(name)
        headers.fetch(header_names.fetch(name.downcase, nil), [].freeze)
      end

      def without_header(name)
        message = clone
        norm = name.downcase

        message.headers = message.headers.except(header_names.fetch(norm, nil))
        self.header_names = header_names.except(norm)

        message
      end

      def with_body(stream)
        message = clone
        message.body = stream

        message
      end

      # @return [String]
      attr_reader(:protocol_version)
      # @return [Hash{String=>Array<String>}]
      attr_reader(:headers)
      # @return [IO]
      attr_reader(:body)

      protected

        attr_writer(:protocol_version)
        attr_writer(:headers)
        attr_writer(:body)
        attr_accessor(:header_names)

        def header_name_for(name)
          norm = name.downcase

          if header_names.key?(norm)
            header_names.fetch(norm)
          else
            self.header_names = { **header_names, norm => name }

            name
          end
        end

        def valid_name(value)
          value.is_a?(String) && value != '' ||
            value.is_a?(Array) && value.any?
        end

        def valid_value(value)
          value.is_a?(String) ||
            value.is_a?(Array) && value.any?
        end
    end
  end
end
