# frozen_string_literal: true

module Corindon
  module Http
    class Request < Message
      include(Interfaces::Request)

      attr_reader(:request_target)
      attr_reader(:method)
      attr_reader(:uri)

      def initialize(
        protocol_version: '1.0',
        headers: {}.freeze,
        body: StringIO.new,
        request_target: '/',
        method: 'GET',
        uri: URI.parse('')
      )
        super(
          protocol_version: protocol_version,
          headers: headers,
          body: body,
        )

        self.request_target = request_target
        self.method = method
        self.uri = uri
      end

      def eql?(other)
        super(other) &&
          request_target == other.request_target &&
          method == other.method &&
          uri == other.uri
      end

      def with_request_target(target)
        message = clone
        message.request_target = target

        message
      end

      def with_method(method)
        if method.is_a?(String)
          message = clone
          message.method = method

          message
        else
          raise(ArgumentError.new("The method '#{method}' is not valid"))
        end
      end

      # @param [URI] uri
      def with_uri(uri, preserve_host: false)
        message = clone
        message.uri = uri

        if (!preserve_host || header_line('host') == '') && !uri.host.nil?
          message.with_header('Host', uri.host)
        else
          message
        end
      end

      protected

        attr_writer(:request_target)
        attr_writer(:method)
        attr_writer(:uri)
    end
  end
end
