# frozen_string_literal: true

module Corindon
  module Http
    class Application
      # @return [DependencyInjection::Container]
      attr_reader(:container)

      def initialize(container: DependencyInjection::Container.new)
        @router = Hanami::Router.new
        @middlewares = []
        @container = container
      end

      def handle(env)
        @router.call(env)
      end

      def call(env)
        handle(env)
      end

      def get(url, **injection_tokens, &block)
        @router.get(url, to: ->(env) {
          handle_exception do
            request = create_request_from_env(env)

            convert_to_rack_response(request, tokens: injection_tokens, &block)
          end
        })
      end

      def post(url, &block)
        @router.post(url, to: ->(env) {
          handle_exception do
            request = create_request_from_env(env)

            convert_to_rack_response(request, &block)
          end
        })
      end

      def use(middleware)
        @middlewares.push(middleware)
      end

      private

        def create_request_from_env(env)
          request = ServerRequest.from_rack_env(env)

          request = request.with_attribute('container', container)

          if env.key?('router.params')
            request = request.with_query_params(
              **request.query_params,
              **env.fetch('router.params')
            )
          end

          request
        end

        def call_with_middlewares(middlewares, request, tokens: {}, &block)
          if middlewares.empty?
            kwargs = block.parameters
                          .filter { |type, _name| %i[key keyreq].include?(type) }
                          .map { |_k, v| v }

            selected_qps = request.query_params
                                  .transform_keys(&:to_sym)
                                  .slice(*kwargs)

            services = tokens.transform_values { |token| container.get(token) }

            block.call(request, **selected_qps, **services)
          else
            middleware, *rest = middlewares

            middleware.handle(request) do |next_req|
              call_with_middlewares(rest, next_req, tokens: tokens, &block)
            end
          end
        end

        def convert_to_rack_response(request, tokens: {}, &block)
          response = call_with_middlewares(@middlewares, request, tokens: tokens, &block)

          if response.is_a?(Interfaces::Response)
            [response.status, response.headers, response.body]
          else
            [500, {}, []]
          end
        end

        def handle_exception(&block)
          block.call
        rescue StandardError => e
          [500, {}, [e.message, "\n", e.backtrace.join("\n")]]
        end
    end
  end
end
