# frozen_string_literal: true

require('corindon')
require('hanami/router')
require('rack')
require('zeitwerk')

loader = Zeitwerk::Loader.new
loader.tag = "corindon-#{File.basename(__FILE__, '.rb')}"
loader.inflector = Zeitwerk::GemInflector.new(__FILE__)
loader.push_dir(__dir__, namespace: Corindon)

loader.setup
